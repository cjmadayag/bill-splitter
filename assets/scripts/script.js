const bill = document.getElementById("bill");
const friendInput = document.getElementById("friend");
const submitBtn = document.getElementById("submitBtn");
const share = document.getElementById("shareContent");
const currentBill = document.getElementById("currentBill");
const friendShare = document.getElementById("friendShare");

let friendsList=[];

submitBtn.addEventListener("click",function(){
	let friends={
		name,
		share
	};
	if(bill.value===""){
		return alert("Enter bill");
	}

	if(friends.length===0 && friendInput.value===""){
		return alert("Add friend");
	}

	if(friendShare.value===""){
		return alert("Enter share");
	}
	
	if (parseInt(bill.value)>0 && parseInt(friendShare.value)>0){
		friends.name = friendInput.value;
		friends.share= parseInt(friendShare.value);
		friendsList.push(friends);
		updateShare();
	}
	else{
		return alert("Enter correct value");
	}
})

bill.addEventListener("keyup", function(){
	currentBill.textContent = bill.value;

	if(friendsList.lenth!==0){
		updateShare();
	}
})

function updateShare(){
	let computeShare;
	let additionalShare=0;

	if(friendsList.length===1){
		additionalShare+=friendsList[0].share;
		computeShare = parseInt(bill.value)-additionalShare;
	}else{
		friendsList.forEach(function(friend){
			additionalShare+=friend.share;
		})
		computeShare = (parseInt(bill.value)-additionalShare)/friendsList.length;
	}
	
	share.innerHTML=""

	friendsList.forEach(function(friend,index){
		const h = document.createElement("h3");
		const b = document.createElement("button");

		h.textContent=friend.name+ " : " + (computeShare+friend.share).toFixed(2) + " ";
		h.setAttribute("id",index);

		b.classList.add("btn","btn-danger");
		b.setAttribute("onclick","deleteFunction("+index+")");
		
		b.textContent="Delete";


		h.appendChild(b);

		share.appendChild(h);
		console.log(h); 
	})
}

function deleteFunction(index){
	const del = document.getElementById(index);
	del.parentNode.removeChild(del);

	friendsList.splice(index,1);
	updateShare();
}


// old_element.parentNode.removeChild(old_element);